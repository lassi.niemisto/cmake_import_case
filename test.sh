#!/bin/sh

CMAKE_CMD=/opt/devbox-cmake/3.13/bin/cmake

rm -rf /tmp/cmake_test_system_lassi
cp -R cmake_test_system_lassi /tmp/

# Build the extlib
cd /tmp/cmake_test_system_lassi/extlib
mkdir build
cd build
$CMAKE_CMD ../
make

# Build tree1
cd /tmp/cmake_test_system_lassi/tree1
mkdir build
cd build
$CMAKE_CMD ../
make
make install

# Build tree2
cd /tmp/cmake_test_system_lassi/tree2
mkdir build
cd build
$CMAKE_CMD ../
make

